open Relude.Globals;

let exnToString = e => {
  let name = Js.Exn.name(e) |> Option.getOrElse("<NO_NAME>");
  let filename = Js.Exn.fileName(e) |> Option.getOrElse("<NO_FILENAME>");
  let message = Js.Exn.message(e) |> Option.getOrElse("<NO_MESSAGE>");

  {j|$name :: $filename ::\n$message|j};
};

Ava.test("Test base64 encoding using node docs example", t => {
  let expected = "aGVsbG8gd29ybGQ=";

  Node.Buffer.fromStringWithEncoding("hello world", `ascii)
  |> Painless.Base64.encode
  |> Result.tap(actual => Ava.Assert.is(t, actual, expected))
  |> Result.tapError(e =>
       Ava.Test.fail(~message=exnToString(e), t) |> ignore
     )
  |> ignore;
});

Ava.test("Test base64 decoding using node docs example", t => {
  let encoded = "aGVsbG8gd29ybGQ=";
  let expected = "hello world";

  Painless.Base64.decode(encoded)
  |> Result.map(Node.Buffer.toString)
  |> Result.tap(actual => Ava.Assert.is(t, actual, expected))
  |> Result.tapError(e =>
       Ava.Test.fail(~message=exnToString(e), t) |> ignore
     )
  |> ignore;
});

Ava.test("Test base58 encoding using the bs58 docs example", t => {
  let address = "003c176e659bea0f29a3e9bf7880c112b1b31b4dc826268187";
  let expected = "16UjcYNBG9GTK4uq2f7yYEbuifqCzoLMGS";

  Node.Buffer.fromStringWithEncoding(address, `hex)
  |> Painless.Base58.encode
  |> Result.tap(actual => Ava.Assert.is(t, actual, expected))
  |> Result.tapError(e =>
       Ava.Test.fail(~message=exnToString(e), t) |> ignore
     )
  |> ignore;
});

Ava.test("Test base58 decoding using the bs58 docs example", t => {
  let encoded = "16UjcYNBG9GTK4uq2f7yYEbuifqCzoLMGS";
  let expected = "003c176e659bea0f29a3e9bf7880c112b1b31b4dc826268187";

  Painless.Base58.decode(encoded)
  |> Result.map(buffer => NodeCrypto.Native.Buffer.toString(buffer, `hex))
  |> Result.tap(actual => Ava.Assert.is(t, actual, expected))
  |> Result.tapError(e =>
       Ava.Test.fail(~message=exnToString(e), t) |> ignore
     )
  |> ignore;
});
