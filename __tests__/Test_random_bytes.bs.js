// Generated by BUCKLESCRIPT VERSION 5.0.6, PLEASE EDIT WITH CARE
'use strict';

var Ava = require("@buckeye/bs-ava/src/Ava.bs.js");
var Painless = require("../src/Painless.bs.js");

Ava.test("Test randomBytes returns correct number of bytes", (function (t) {
        var bytes = Painless.randomBytes(16);
        var byteCount = bytes.length;
        return Ava.Assert[/* is */4](undefined, t, byteCount, 16);
      }));

/*  Not a pure module */
