open Relude.Globals;

// These tests are pulled from the [`node-scrypt`] lib [tests][node-scrypt-tests].
// https://github.com/barrysteyn/node-scrypt/blob/8c7e3fce9efbab2bc0eab677721483870002f76e/tests/scrypt-tests.js#L61
// This implementation uses the [built-in Node.js scrypt wrapper][node-crypto-scrypt]
// Here is the documentation for the built-in Node.js scrypt wrapper:
//
// [node-crypto-scrypt]: https://nodejs.org/api/crypto.html#crypto_crypto_scrypt_password_salt_keylen_options_callback
// [node-scrypt]: https://github.com/barrysteyn/node-scrypt
// [node-scrypt-tests]: https://github.com/barrysteyn/node-scrypt/blob/8c7e3fce9efbab2bc0eab677721483870002f76e/tests/scrypt-tests.js#L61

let exnToString = e => {
  let name = Js.Exn.name(e) |> Option.getOrElse("<NO_NAME>");
  let filename = Js.Exn.fileName(e) |> Option.getOrElse("<NO_FILENAME>");
  let message = Js.Exn.message(e) |> Option.getOrElse("<NO_MESSAGE>");

  {j|$name :: $filename ::\n$message|j};
};

// This will generate a test as prescribed in the [IETF scrypt paper][ietf-scrypt].
// [ietf-scrypt]: https://tools.ietf.org/html/draft-josefsson-scrypt-kdf-00#page-11
let makePaperTest =
    (
      ~vectorNumber,
      ~cost,
      ~blockSize,
      ~parallelization,
      ~keylength,
      ~salt,
      ~key,
      ~expected,
    ) =>
  Ava.Async.test(
    Ava.ava,
    {j|Vector $vectorNumber: will produce an identical vector to the IETF scrypt paper|j},
    t =>
    Painless.scrypt(
      ~cost,
      ~blockSize,
      ~parallelization,
      ~keylength,
      key,
      salt,
    )
    |> IO.mapError(exnToString)
    |> IO.unsafeRunAsync(result =>
         result
         |> Result.tapError(e => Ava.Test.fail(~message=e, t))
         |> Result.tap(Ava.Assert.is(t, expected))
         |> Result.fold(_ => Ava.Test.finish(t), _ => Ava.Test.finish(t))
       )
  );

makePaperTest(
  ~vectorNumber="1",
  ~cost=16,
  ~blockSize=1,
  ~parallelization=1,
  ~keylength=64,
  ~salt="",
  ~key="",
  ~expected=
    "77d6576238657b203b19ca42c18a0497f16b4844e3074ae8dfdffa3fede21442fcd0069ded0948f8326a753a0fc81f17e8d3e0fb2e0d3628cf35e20c38d18906",
);

makePaperTest(
  ~vectorNumber="2",
  ~cost=1024,
  ~blockSize=8,
  ~parallelization=16,
  ~keylength=64,
  ~salt="NaCl",
  ~key="password",
  ~expected=
    "fdbabe1c9d3472007856e7190d01e9fe7c6ad7cbc8237830e77376634b3731622eaf30d92e22a3886ff109279d9830dac727afb94a83ee6d8360cbdfa2cc0640",
);

makePaperTest(
  ~vectorNumber="3",
  ~cost=16384,
  ~blockSize=8,
  ~parallelization=1,
  ~keylength=64,
  ~salt="SodiumChloride",
  ~key="pleaseletmein",
  ~expected=
    "7023bdcb3afd7348461c06cd81fd38ebfda8fbba904f8e3ea9b543f6545da1f2d5432955613f0fcf62d49705242a9af9e61e85dc0d651e40dfcf017b45575887",
);

/*
  * This test causes a memory limit exceeded exception
 makePaperTest(
   ~vectorNumber="4",
   ~cost=1048576,
   ~blockSize=8,
   ~parallelization=1,
   ~keylength=64,
   ~salt="SodiumChloride",
   ~key="pleaseletmein",
   ~expected=
     "2101cb9b6a511aaeaddbbe09cf70f881ec568d574a2ffd4dabe5ee9820adaa478e56fd8f4ba5d09ffa1c6d927c40f4c337304049e8a952fbcbf45c6fa77a41a4",
 );
 */
