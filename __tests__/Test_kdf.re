open Relude.Globals;

let exnToString = e => {
  let name = Js.Exn.name(e) |> Option.getOrElse("<NO_NAME>");
  let filename = Js.Exn.fileName(e) |> Option.getOrElse("<NO_FILENAME>");
  let message = Js.Exn.message(e) |> Option.getOrElse("<NO_MESSAGE>");

  {j|$name :: $filename ::\n$message|j};
};

Ava.Async.test(
  Ava.ava,
  "KDF :: keyMake / keyVerify",
  t => {
    let password = "This is my password";

    Painless.keyMake(password)
    |> IO.flatMap(key => Painless.keyVerify(key, password))
    |> IO.mapError(exnToString)
    |> IO.unsafeRunAsync(result =>
         result
         |> Result.tapError(e => Ava.Test.fail(~message=e, t))
         |> Result.tap(Ava.Assert.true_(t))
         |> Result.fold(_ => Ava.Test.finish(t), _ => Ava.Test.finish(t))
       );
  },
);
