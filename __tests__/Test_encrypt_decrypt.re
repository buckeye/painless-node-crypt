open Relude.Globals;

let exnToString = e => {
  let name = Js.Exn.name(e) |> Option.getOrElse("<NO_NAME>");
  let filename = Js.Exn.fileName(e) |> Option.getOrElse("<NO_FILENAME>");
  let message = Js.Exn.message(e) |> Option.getOrElse("<NO_MESSAGE>");

  {j|$name :: $filename ::\n$message|j};
};

Ava.test("Encrypt / Decrypt a string value", t => {
  let secret = "This is my secret";
  let key = "ThisIsMyFancyKey";

  let verifyEncrypted = string => Ava.Assert.isnt(t, secret, string);
  let verifyDecrypted = string => Ava.Assert.is(t, secret, string);

  Painless.encrypt(key, secret)
  |> Result.tap(verifyEncrypted)
  |> Result.flatMap(encrypted => Painless.decrypt(key, encrypted))
  |> Result.tap(verifyDecrypted)
  |> Result.tapError(e =>
       Ava.Test.fail(~message=exnToString(e), t) |> ignore
     )
  |> ignore;
});
