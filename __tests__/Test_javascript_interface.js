const test = require("ava");
const Painless = require("../dist/index");

test("Encrypt / Decrypt a string value", async t => {
  const plaintext = "This is my JS secret";
  const key = "This is some key";

  try {
    const ciphertext = await Painless.encrypt(key, plaintext);
    const decrypted = await Painless.decrypt(key, ciphertext);

    t.not(ciphertext, plaintext, "encryption didn't really do anything");
    t.is(decrypted, plaintext, "decryption failed");
  } catch (error) {
    t.fail(String(error));
  }
});

const scrypt_test_vectors = [
  {
    name: "1",
    cost: 16,
    blockSize: 1,
    parallelization: 1,
    keylength: 64,
    salt: "",
    key: "",
    expected:
      "77d6576238657b203b19ca42c18a0497f16b4844e3074ae8dfdffa3fede21442fcd0069ded0948f8326a753a0fc81f17e8d3e0fb2e0d3628cf35e20c38d18906"
  },
  {
    name: "2",
    cost: 1024,
    blockSize: 8,
    parallelization: 16,
    keylength: 64,
    salt: "NaCl",
    key: "password",
    expected:
      "fdbabe1c9d3472007856e7190d01e9fe7c6ad7cbc8237830e77376634b3731622eaf30d92e22a3886ff109279d9830dac727afb94a83ee6d8360cbdfa2cc0640"
  },
  {
    name: "3",
    cost: 16384,
    blockSize: 8,
    parallelization: 1,
    keylength: 64,
    salt: "SodiumChloride",
    key: "pleaseletmein",
    expected:
      "7023bdcb3afd7348461c06cd81fd38ebfda8fbba904f8e3ea9b543f6545da1f2d5432955613f0fcf62d49705242a9af9e61e85dc0d651e40dfcf017b45575887"
  }
];

scrypt_test_vectors.map(config =>
  test(`IETF test vector ${config.name} should produce an identical vector`, async t => {
    try {
      const vector = await Painless.scrypt(config);
      t.is(vector, config.expected);
    } catch (error) {
      t.fail(String(error));
    }
  })
);

test("KDF :: keyMake / keyVerify", async t => {
  try {
    const password = "This is my password";

    const key = await Painless.keyMake({ password });
    const isValid = await Painless.keyVerify({ key, password });

    t.true(isValid);
  } catch (error) {
    t.fail(String(error));
  }
});

test("randomBytes returns the correct byte count", t => {
  const bytes = Painless.randomBytes(32);
  t.is(bytes.length, 32);
});

test("base58 encoding", async t => {
  const address = "003c176e659bea0f29a3e9bf7880c112b1b31b4dc826268187";
  const expected = "16UjcYNBG9GTK4uq2f7yYEbuifqCzoLMGS";
  const input = Buffer.from(address, "hex");
  const actual = await Painless.base58Encode(input);

  t.is(actual, expected);
});

test("base58 decoding", async t => {
  const encoded = "16UjcYNBG9GTK4uq2f7yYEbuifqCzoLMGS";
  const expected = "003c176e659bea0f29a3e9bf7880c112b1b31b4dc826268187";
  const actual = await Painless.base58Decode(encoded);

  t.is(actual.toString("hex"), expected);
});

test("base64 encoding", async t => {
  const expected = "aGVsbG8gd29ybGQ=";
  const input = Buffer.from("hello world", "ascii");
  const actual = await Painless.base64Encode(input);

  t.is(actual, expected);
});

test("base64 decoding", async t => {
  const encoded = "aGVsbG8gd29ybGQ=";
  const expected = "hello world";
  const actual = await Painless.base64Decode(encoded);

  t.is(actual.toString("ascii"), expected);
});
