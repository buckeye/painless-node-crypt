[@bs.get] external bufferLength: Node.Buffer.t => int = "length";

Ava.test("Test randomBytes returns correct number of bytes", t => {
  let bytes = Painless.randomBytes(16);
  let byteCount = bufferLength(bytes);

  Ava.Assert.is(t, byteCount, 16);
});
