open Relude.Globals;

let encrypt =
  (. key, plaintext) =>
    switch (Painless.encrypt(key, plaintext)) {
    | Ok(ciphertext) => Js.Promise.resolve(ciphertext)
    | Error(err) => Js.Promise.reject(err |> RJs.Exn.unsafeToExn)
    };

let decrypt =
  (. key, ciphertext) =>
    switch (Painless.decrypt(key, ciphertext)) {
    | Ok(plaintext) => Js.Promise.resolve(plaintext)
    | Error(err) => Js.Promise.reject(err |> RJs.Exn.unsafeToExn)
    };

module ScryptRequest = {
  [@bs.deriving abstract]
  type t = {
    [@bs.optional]
    cost: int,
    [@bs.optional]
    blockSize: int,
    [@bs.optional]
    parallelization: int,
    [@bs.optional]
    keylength: int,
    key: string,
    salt: string,
  };
};

let scrypt =
  (. request) =>
    Painless.scrypt(
      ~cost=?ScryptRequest.costGet(request),
      ~blockSize=?ScryptRequest.blockSizeGet(request),
      ~parallelization=?ScryptRequest.parallelizationGet(request),
      ~keylength=?ScryptRequest.keylengthGet(request),
      ScryptRequest.keyGet(request),
      ScryptRequest.saltGet(request),
    )
    |> RJs.Promise.fromIO;

module KDFRequest = {
  [@bs.deriving abstract]
  type t = {
    [@bs.optional]
    cost: int,
    [@bs.optional]
    blockSize: int,
    [@bs.optional]
    parallelization: int,
    [@bs.optional]
    keylength: int,
    [@bs.optional]
    salt: string,
    password: string,
  };
};

let keyMake =
  (. request) =>
    Painless.keyMake(
      ~cost=?KDFRequest.costGet(request),
      ~blockSize=?KDFRequest.blockSizeGet(request),
      ~parallelization=?KDFRequest.parallelizationGet(request),
      ~keylength=?KDFRequest.keylengthGet(request),
      ~salt=?
        KDFRequest.saltGet(request)
        |> Option.map(NodeCrypto.Native.Buffer.fromUtf8),
      KDFRequest.passwordGet(request),
    )
    |> RJs.Promise.fromIO;

module KDFVerifyRequest = {
  [@bs.deriving abstract]
  type t = {
    key: string,
    password: string,
  };
};

let keyVerify =
  (. request) =>
    Painless.keyVerify(
      KDFVerifyRequest.keyGet(request),
      KDFVerifyRequest.passwordGet(request),
    )
    |> RJs.Promise.fromIO;

let randomBytes = (. byteCount) => Painless.randomBytes(byteCount);

let base58Encode =
  (. buffer) =>
    switch (Painless.Base58.encode(buffer)) {
    | Ok(encoded) => Js.Promise.resolve(encoded)
    | Error(err) => Js.Promise.reject(err |> RJs.Exn.unsafeToExn)
    };

let base58Decode =
  (. string) =>
    switch (Painless.Base58.decode(string)) {
    | Ok(string) => Js.Promise.resolve(string)
    | Error(err) => Js.Promise.reject(err |> RJs.Exn.unsafeToExn)
    };

let base64Encode =
  (. buffer) =>
    switch (Painless.Base64.encode(buffer)) {
    | Ok(encoded) => Js.Promise.resolve(encoded)
    | Error(err) => Js.Promise.reject(err |> RJs.Exn.unsafeToExn)
    };

let base64Decode =
  (. string) =>
    switch (Painless.Base64.decode(string)) {
    | Ok(string) => Js.Promise.resolve(string)
    | Error(err) => Js.Promise.reject(err |> RJs.Exn.unsafeToExn)
    };
