open Relude.Globals;

let invalidBufferContents: unit => Js.Exn.t = [%bs.raw
  {|
  function() {
    return new Error("Invalid encrypted buffer contents");
  }
|}
];

let unknownScryptResult: unit => Js.Exn.t = [%bs.raw
  {|
function() {
  return new Error("Scrypt attempt returned and empty key and empty error");
}
|}
];

let bufferToHex = buffer => NodeCrypto.Native.Buffer.toString(buffer, `hex);

let hexToBuffer = hex => NodeCrypto.Native.Buffer.fromHex(hex);

let encrypt = (key, secret) => {
  let iv = NodeCrypto.Random.bytes(16);
  let salt = NodeCrypto.Random.bytes(16);

  NodeCrypto.CipherIV.encrypt(
    `aes256cbc,
    `string(key),
    `buffer(salt),
    `buffer(iv),
    `string(secret),
  )
  |> Result.map(encryptedBuffer => [|salt, encryptedBuffer, iv|])
  |> Result.map(Array.map(bufferToHex))
  |> Result.map(Array.String.joinWith("."))
  |> Result.map(NodeCrypto.Native.Buffer.fromUtf8)
  |> Result.map(buf => NodeCrypto.Native.Buffer.toString(buf, `base64));
};

let decrypt = (key, encBuffer) => {
  let parts =
    Node.Buffer.fromStringWithEncoding(encBuffer, `base64)
    |> Node.Buffer.toString
    |> String.splitArray(~delimiter=".")
    |> Array.map(hexToBuffer);

  let decrypt = (salt, encBuffer, iv) =>
    NodeCrypto.DecipherIV.decrypt(
      `aes256cbc,
      `string(key),
      `buffer(salt),
      `buffer(iv),
      `buffer(encBuffer),
    )
    |> Result.map(Node.Buffer.toString);

  switch (parts) {
  | [|salt, encBuffer, iv|] => decrypt(salt, encBuffer, iv)
  | _ => invalidBufferContents() |> Result.error
  };
};

module NodeScrypt = {
  module Options = {
    [@bs.deriving abstract]
    type t = {
      [@bs.optional]
      cost: int,
      [@bs.optional]
      blockSize: int,
      [@bs.optional]
      parallelization: int,
      //Memory upper bound.
      // It is an error when (approximately) 128 * N * r > maxmem.
      // Default: 32 * 1024 * 1024 (works up to 16,384 (2^14)
      [@bs.optional]
      maxmem: int,
    };
  };

  [@bs.module "crypto"]
  external scrypt:
    (
      Node.Buffer.t,
      Node.Buffer.t,
      int,
      Options.t,
      (Js.Null_undefined.t(Js.Exn.t), Js.Null_undefined.t(Node.Buffer.t)) =>
      unit
    ) =>
    unit =
    "scrypt";

  let scrypt = (key, salt, keylength, options) =>
    IO.async(complete =>
      scrypt(
        key,
        salt,
        keylength,
        options,
        (err, derivedKey) => {
          let err = Js.Null_undefined.toOption(err);
          let derivedKey = Js.Null_undefined.toOption(derivedKey);

          let result =
            switch (err, derivedKey) {
            | (Some(e), _) => Result.error(e)
            | (None, None) => unknownScryptResult() |> Result.error
            | (None, Some(key)) => Result.ok(key)
            };

          complete(result);
        },
      )
    );
};

let scrypt =
    (~cost=32768, ~blockSize=8, ~parallelization=2, ~keylength=64, key, salt) => {
  let key = NodeCrypto.Native.Buffer.fromUtf8(key);
  let salt = NodeCrypto.Native.Buffer.fromUtf8(salt);

  let options = NodeScrypt.Options.t(~cost, ~blockSize, ~parallelization, ());

  NodeScrypt.scrypt(key, salt, keylength, options)
  |> IO.map(buffer => NodeCrypto.Native.Buffer.toString(buffer, `hex));
};
let calcMaxMem = (cost, blockSize) => {
  let defaultMaxMem = int_of_float(2.0 ** 14.0);

  if (cost > defaultMaxMem) {
    // maxmem check does an exclusive compare to 128 * cost * blockSize, so we
    // use * 2 (128 *2 = 256) to ensure enough memory.
    256 * cost * blockSize;
  } else {
    // Default setting within Node.crypto
    32 * 1024 * 1024;
  };
};

let keyMake =
    (
      ~cost=32768,
      ~blockSize=8,
      ~parallelization=2,
      ~keylength=64,
      ~salt=?,
      password,
    ) => {
  let salt = salt |> Option.getOrElseLazy(() => NodeCrypto.Random.bytes(64));
  let key = NodeCrypto.Native.Buffer.fromUtf8(password);
  // maxmem check does an exclusive compare to 128 * cost * blockSize, so we
  // use * 2 (128 *2 = 256) to ensure enough memory.
  let maxmem = calcMaxMem(cost, blockSize);

  let options =
    NodeScrypt.Options.t(~cost, ~blockSize, ~parallelization, ~maxmem, ());

  NodeScrypt.scrypt(key, salt, keylength, options)
  |> IO.map(bufferToHex)
  |> IO.map(key => {
       let salt = bufferToHex(salt);
       let cost = string_of_int(cost);
       let keylength = string_of_int(keylength);
       let key = {j|$salt.$key.$cost.$blockSize.$parallelization.$keylength|j};

       NodeCrypto.Native.Buffer.fromUtf8(key);
     })
  |> IO.map(buffer => NodeCrypto.Native.Buffer.toString(buffer, `base64));
};

let keyVerify = (key, password) => {
  let passwordBuffer = NodeCrypto.Native.Buffer.fromUtf8(password);
  let parts =
    Node.Buffer.fromStringWithEncoding(key, `base64)
    |> Node.Buffer.toString
    |> String.splitArray(~delimiter=".");

  let verify = (salt, key, cost, blockSize, parallelization, keylength) => {
    let cost = int_of_string(cost);
    let blockSize = int_of_string(blockSize);
    let parallelization = int_of_string(parallelization);
    let keylength = int_of_string(keylength);

    let options =
      NodeScrypt.Options.t(
        ~cost,
        ~blockSize,
        ~parallelization,
        ~maxmem=calcMaxMem(cost, blockSize),
        (),
      );

    NodeScrypt.scrypt(passwordBuffer, hexToBuffer(salt), keylength, options)
    |> IO.map(bufferToHex)
    |> IO.map(passwordKey => passwordKey == key);
  };

  // This will allow version 1.x hashed items to continue to work.
  let verify_v1 = (salt, key, cost, keylength) => {
    let blockSize = string_of_int(8);
    let parallelization = string_of_int(1);

    verify(salt, key, cost, blockSize, parallelization, keylength);
  };

  switch (parts) {
  | [|salt, key, cost, blockSize, parallelization, keylength|] =>
    verify(salt, key, cost, blockSize, parallelization, keylength)
  | [|salt, key, cost, keylength|] => verify_v1(salt, key, cost, keylength)
  | _ => invalidBufferContents() |> Result.error |> IO.fromResult
  };
};

let randomBytes = byteCount => NodeCrypto.Random.bytes(byteCount);

module Base58 = {
  [@bs.module "bs58"] external encode: Node.Buffer.t => string = "encode";

  [@bs.module "bs58"] external decode: string => Node.Buffer.t = "decode";

  let encode = buffer =>
    try (buffer |> encode |> Result.ok) {
    | Js.Exn.Error(error) => error |> Result.error
    };

  let decode = string =>
    try (string |> decode |> Result.ok) {
    | Js.Exn.Error(error) => error |> Result.error
    };
};

module Base64 = {
  let encode = buffer =>
    try (NodeCrypto.Native.Buffer.toString(buffer, `base64) |> Result.ok) {
    | Js.Exn.Error(error) => error |> Result.error
    };

  let decode = string =>
    try (Node.Buffer.fromStringWithEncoding(string, `base64) |> Result.ok) {
    | Js.Exn.Error(error) => error |> Result.error
    };
};
