let encrypt: (. string, string) => Js.Promise.t(string);

let decrypt: (. string, string) => Js.Promise.t(string);

module ScryptRequest: {
  [@bs.deriving abstract]
  type t = {
    [@bs.optional]
    cost: int,
    [@bs.optional]
    blockSize: int,
    [@bs.optional]
    parallelization: int,
    [@bs.optional]
    keylength: int,
    key: string,
    salt: string,
  };
};

let scrypt: (. ScryptRequest.t) => Js.Promise.t(string);

module KDFRequest: {
  [@bs.deriving abstract]
  type t = {
    [@bs.optional]
    cost: int,
    [@bs.optional]
    blockSize: int,
    [@bs.optional]
    parallelization: int,
    [@bs.optional]
    keylength: int,
    [@bs.optional]
    salt: string,
    password: string,
  };
};

let keyMake: (. KDFRequest.t) => Js.Promise.t(string);

module KDFVerifyRequest: {
  [@bs.deriving abstract]
  type t = {
    key: string,
    password: string,
  };
};

let keyVerify: (. KDFVerifyRequest.t) => Js.Promise.t(bool);

let randomBytes: (. int) => Node.Buffer.t;

let base58Encode: (. Node.Buffer.t) => Js.Promise.t(string);

let base58Decode: (. string) => Js.Promise.t(Node.Buffer.t);

let base64Encode: (. Node.Buffer.t) => Js.Promise.t(string);

let base64Decode: (. string) => Js.Promise.t(Node.Buffer.t);
