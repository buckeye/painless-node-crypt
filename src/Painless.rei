let encrypt: (string, string) => Belt.Result.t(string, Js.Exn.t);

let decrypt: (string, string) => Belt.Result.t(string, Js.Exn.t);

let scrypt:
  (
    ~cost: int=?,
    ~blockSize: int=?,
    ~parallelization: int=?,
    ~keylength: int=?,
    string,
    string
  ) =>
  Relude.IO.t(string, Js.Exn.t);

let keyMake:
  (
    ~cost: int=?,
    ~blockSize: int=?,
    ~parallelization: int=?,
    ~keylength: int=?,
    ~salt: Node.Buffer.t=?,
    string
  ) =>
  Relude.IO.t(string, Js.Exn.t);

let keyVerify: (string, string) => Relude.IO.t(bool, Js.Exn.t);

let randomBytes: int => Node.Buffer.t;

module Base58: {
  let encode: Node.Buffer.t => Belt.Result.t(string, Js.Exn.t);

  let decode: string => Belt.Result.t(Node.Buffer.t, Js.Exn.t);
};

module Base64: {
  let encode: Node.Buffer.t => Belt.Result.t(string, Js.Exn.t);

  let decode: string => Belt.Result.t(Node.Buffer.t, Js.Exn.t);
};
