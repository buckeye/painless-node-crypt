# painless-node-crypt

A BuckleScript wrapper around the Node encryption libraries to make the encryption / decryption more user friendly

## Installation

1. Install necessary packages.

```sh
yarn add bs-abstract relude @buckeye/bs-node-crypto
```

2. Add dependencies to your `bsconfig.json`

```json
{
  "dependencies": ["bs-abstract", "relude", "@buckeye/painless-node-crypt"]
}
```

## Usage

### Symmetric key (shared password) key encryption

This allows you to encrypt / decrypt values based upon a shared secret
key value. This is different than public / private key (asymmetric key)
based encryption.

#### Encrypt

```reasonml
let secret = "A private message";
let password = "shh... this is a secret";

let encrypted =
  switch(Painless.encrypt(password, secret)) {
  | Ok(encrypted) => encrypted
  | Error(e) => raise(e)
  };

```

### Decrypt

```
let encrypted = ""; // value from our the previous example.
let password = "shh... this is a secret";

let message =
  switch(Painless.decrypt(password, encrypted) {
  | Ok(plaintext) => plaintext
  | Error(e) => raise(e)
  }

Js.Console.log("shh... this is a secret" == message);
```

### Scrypt

This allows you to generate an Scrypt hash for a given string value, and
provided salt string value.

```reasonml
let secret = "This is something I want to hash";
let salt = "This is a random or sufficiently random known value";

Painless.scrypt(secret, salt)
|> IO.unsafeRunAsync(result =>
  result
  |> Result.fold(
    error => Js.Console.error(error),
    hash => Js.Console.log(hash)
  )
);
```

### Password Hashing (Key Derivation Function)

This allows for a simple, self-contained way to one-way hash your passwords
using scrypt.

#### Hash Password;

```reasonml
let password = "super safe";

Painless.keyMake(password)
|> IO.unsafeRunAsync(result =>
  result
  |> Result.fold(
    error => Js.Console.error(error),
    hash => Js.Console.log(hash)
  )
)
```

Once you have the password hash, you would securely store the hash in your
database.

#### Password Verification

When the user attempts to log in, you would verify their password like so:

```reasonml
let password = "super safe";
let hash = ""; // string value retrieved from our database.

Painless.keyVerify(hash, password)
|> IO.unsafeRunAsync(result =>
  result
  |> Result.fold(
    error => Js.Console.error(error),
    bool => Js.Console.log2("Password OK:", bool)
  )
```
