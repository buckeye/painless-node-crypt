const {
  encrypt,
  decrypt,
  scrypt,
  keyMake,
  keyVerify,
  randomBytes,
  base58Decode,
  base58Encode,
  base64Decode,
  base64Encode
} = require("./src/Painless_js.bs");

module.exports = {
  encrypt,
  decrypt,
  scrypt,
  keyMake,
  keyVerify,
  randomBytes,
  base58Decode,
  base58Encode,
  base64Decode,
  base64Encode
};
